import {
  frontend,
  backend,
  ux,
  prototyping,
  javascript,
  typescript,
  html,
  css,
  reactjs,
  redux,
  tailwind,
  nodejs,
  git,
  figma,
  docker,
  postgresql,
  rubyrails,
  graphql,
  komikult,
  leaderboard,
  math,
  movie,
  nyeusi,
  space,
  coverhunt,
  dcc,
  kelhel,
  microverse,
  tensorflow,
  python,
  django,
  cplusplus,
  springboot,
  java_log,
  pytorch,
  Rl,
  face,
  compiler,
  incentive,
} from '../assets';

export const navLinks = [
  {
    id: 'about',
    title: 'About',
  },
  {
    id: 'projects',
    title: 'Projects',
  },
  {
    id: 'contact',
    title: 'Contact',
  },
];

const services = [
  {
    title: 'Machine learning Engineer',
    icon: frontend,
  },
  {
    title: 'Software Engineer',
    icon: backend,
  },
  {
    title: 'Back-end Developer',
    icon: ux,
  },
  
];

const technologies = [
  {
    name: 'Python',
    icon: python,
  },
  {
    name: 'Tensorflow',
    icon: tensorflow,
  },
  {
    name: 'PyTorch',
    icon: pytorch,
  },
  {
    name: 'Java',
    icon: java_log,
  },
  {
    name: 'SpringBoot',
    icon: springboot,
  },
  {
    name: 'C++',
    icon: cplusplus,
  },
  {
    name: 'Django',
    icon: django,
  },
  {
    name: 'postgresql',
    icon: postgresql,
  },
  {
    name: 'git',
    icon: git,
  },
];

const experiences = [
  {
    title: 'Machine Learning Engineer',
    company_name: 'EcoMundo',
    icon: coverhunt,
    iconBg: '#333333',
    date: 'Jun 2022 - Sep 2023',
  },
  {
    title: 'Machine Learing Engineer',
    company_name: 'Omniya',
    icon: microverse,
    iconBg: '#333333',
    date: 'May 2020 - Dec 2021',
  },
  {
    title: '(Internship) Software Engineer',
    company_name: 'Syriatel Mobile Telecom',
    icon: kelhel,
    iconBg: '#333333',
    date: 'Jan 2019 - Dec 2019',
  },
];

const projects = [
  {
    id: 'project-1',
    name: 'RL',
    description: 'Reinforement Learning Course.',
    tags: [
      {
        name: 'react',
        color: 'blue-text-gradient',
      },
      {
        name: 'mongodb',
        color: 'green-text-gradient',
      },
      {
        name: 'tailwind',
        color: 'pink-text-gradient',
      },
    ],
    image: Rl,
    repo: 'https://github.com/khalil-Hennara/Rinforcment_learning_course',
    demo: 'https://medium.com/@khalil.hennara.247/introduction-to-reinforcement-learning-fa84c56d7f07',
  },
  {
    id: 'project-2',
    name: 'Face Recognition',
    description:
      'Face recognition application has very dynamic structure',
    tags: [
      {
        name: 'react',
        color: 'blue-text-gradient',
      },
      {
        name: 'restapi',
        color: 'green-text-gradient',
      },
      {
        name: 'scss',
        color: 'pink-text-gradient',
      },
    ],
    image: face,
    repo: 'https://github.com/khalil-Hennara/FaceRecogntion',
  },
  {
    id: 'project-3',
    name: 'Incentive System',
    description: 'An application that provide the abality to provide incentive service to any company',
    tags: [
      {
        name: 'nextjs',
        color: 'blue-text-gradient',
      },
      {
        name: 'supabase',
        color: 'green-text-gradient',
      },
      {
        name: 'css',
        color: 'pink-text-gradient',
      },
    ],
    image: incentive,
    repo: 'https://github.com/khalil-Hennara/Incentive_system',
  },
  {
    id: 'project-4',
    name: 'Compiler',
    description: `create a new language that is similar to the python language`,
    tags: [
      {
        name: 'nextjs',
        color: 'blue-text-gradient',
      },
      {
        name: 'supabase',
        color: 'green-text-gradient',
      },
      {
        name: 'css',
        color: 'pink-text-gradient',
      },
    ],
    image: compiler,
    repo: 'https://github.com/khalil-Hennara/Compiler',
  },
];

export { services, technologies, experiences, projects };
